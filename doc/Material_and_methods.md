
Data
====

Genomic data (fasta files containing CDS sequences and their translation, and GFF annotations) for _Vibrio cholerae_ (assembly ASM674v1) were downloaded from the NCBI FTP site (<ftp://ftp.ncbi.nlm.nih.gov>).

The GFF annotation file was used to establish a correspondence between locus tags present in the fasta files and old locus tags.


Codon counting
==============

For each gene, the codons were counted in the CDS sequence, assuming it to be in-frame.
Along with their codon counts, genes were flagged with information extracted from the fasta headers and their translation into amino-acids: first amino-acid, position of first stop codon, number of stops, whether start and end coordinates were reported as well-bounded or not.

This step was performed using Python 3.8.3, with the help of the Mappy 2.20 ([Li, 2018][1]) and Pandas 1.2.4 ([McKinney, 2010][2]; [Reback _et. al_, 2021][3]) libraries.


Gene filtering
==============

Genes whose CDS did not start with a valid start codon were excluded from further computations.

A valid start codon is one among ATA, ATC, ATG, ATT, CTG, GTG, TTG, according to the genetic code for bacteria, archaea and plastids (translation table 11 provided by the NCBI at <ftp://ftp.ncbi.nlm.nih.gov/entrez/misc/data/gc.prt>).

Among the 3594 genes, four (`VC_RS01260` (`VC0258`), `VC_RS04215`, `VC_RS15175` and `VC_RS15675` (`VC_A0473`)) did not have a valid start codon.
All four genes had a possibly ill-defined start position (start position prefixed by "<" in the header of the fasta file).

The only other gene whose CDS had a potentially ill-defined start position (`VC_RS07425`) started with a valid start codon and contained no internal stop codon.
We assumed this CDS was in frame and not detrimental for the outcome of further analyses.

Among the remaining genes (valid start codon and well defined start position) 69 had a CDS containing internal stop codons.
Given the fact that stop read-through can happen ([Namy _et al._, 2004][4]), we decided to keep those genes nonetheless (Note: What about frame-shifts? This may change what actual codons are. Maybe we should re-do the analyses without the genes containing internal stops to check the results are similar?).

Given the above considerations, further computations were performed on the 3590 genes that had a valid start codon.


Codon usage bias computation
============================

The global codon counts were computed for each codon by summing over the above selected genes.

For each gene as well as for the global total, the codons were grouped by encoded amino-acid (with also a group for the stop codons).
Within each group, the proportion of each codon was computed by dividing its count by the sum of the counts of the codons in the group.

The codon usage bias for a given codon and a given gene was then computed by subtracting the corresponding proportion obtained from the global counts from the proportion obtained for this gene.

Codon usage biases were then standardized by dividing each of the above difference by the standard deviation of these differences across all genes, resulting in standardized codon usage biases "by amino-acid" ("SCUB by aa" in short).

All these computations were performed using the already mentioned Pandas 1.2.4 Python library.


Associating genes to their preferred codon
==========================================

For each codon group, genes were associated to the codon for which they had the highest "SCUB by aa" value.
This defined a series of gene clusters denoted using the `"{aa}_{codon}"` pattern.
For instance, `"*_TGA"` contains the genes for which TGA is the codon with the highest standardized usage bias among stop codons.


Extracting most positively biased genes from each cluster
=========================================================

Within each cluster, the distribution of "SCUB by aa" values for each codon was represented using violin plots.
Visual inspections of these violin plots revealed that in most cases, the distribution was multi-modal.

An automated method was devised to further extract from a given cluster the genes corresponding to the sub-group with the highest "SCUB by aa" for the corresponding codon.

This was done by estimating a density distribution for "SCUB by aa" values using a Gaussian Kernel Density Estimate and finding a minimum in this distribution.
The location of this minimum was used as a threshold above which genes were considered to belong to the most positively biased genes.

This was done using the SciPy 1.7.0 ([Virtanen _et. al_, 2020][5]) Python library.

Violin plots were generated using the Matplotlib 3.4.2 ([Hunter, 2007][6]) and Seaborn 0.11.1 ([Waskom, 2021][7]) Python libraries.


Code availability
=================

All code to perform these analyses was implemented in the form of Python scripts, Jupyter notebooks ([Kluyver _et al._, 2016][8]) and Snakemake ([Mölder _et al._, 2021][9]) workflows, and are available in the following git repository:
<https://gitlab.pasteur.fr/bli/17009_only>

[1]: <https://doi.org/10.1093/bioinformatics/bty191> (Li, H. Minimap2: pairwise alignment for nucleotide sequences. _Bioinformatics_. 2018; **34**:3094--3100)

[2]: <https://conference.scipy.org/proceedings/scipy2010/pdfs/mckinney.pdf> (McKinney, W. Data Structures for Statistical Computing in Python. In: Stéfan van der Walt, Jarrod Millman, editors. _Proceedings of the 9th Python in Science Conference_. 2010; 56--61)

[3]: <https://doi.org/10.5281/zenodo.4681666> (Reback J, McKinney W, jbrockmendel, den Bossche JV, Augspurger T, Cloud P, et al. pandas-dev/pandas: Pandas 1.2.4; 2021)

[4]: <https://doi.org/10.1016/S1097-2765(04)00031-0> (Namy O, Rousset J-P, Napthine S, and Brierly I. Reprogrammed Genetic Decoding in Cellular Gene Expression. _Molecular Cell_. 2004; **13**:157--168)

[5]: <https://doi.org/10.1038/s41592-019-0686-2> (Virtanen P, Gommers R, Oliphant TE, Haberland M, Reddy T, Cournapeau D, et al. SciPy 1.0: Fundamental Algorithms for Scientific Computing in Python. _Nature Methods_. 2020; *17*:261--272)

[6]: <https://doi.org/10.1109/MCSE.2007.55> (Hunter JD. Matplotlib: A 2D Graphics Environment. _Computing in Science & Engineering_. 2007; **9**:90--95)

[7]: <https://doi.org/10.21105/joss.03021> (Waskom ML. Seaborn: Statistical Data Visualization. _Journal of Open Source Software_. 2021; **6**:3021)

[8]: <https://doi.org/10.3233/978-1-61499-649-1-87> (Kluyver T, Ragan-Kelley B, Pérez F, Granger B, Bussonnier M, Frederic J, et al. Jupyter Notebooks -- a publishing format for reproducible computational workflows. In: Loizides Fernando, Scmidt Birgit, editors _Positioning and Power in Academic Publishing: Players, Agents and Agendas_. 2016; 87--90)

[9]: <https://doi.org/10.12688/f1000research.29032.2> (Mölder F, Jablonski KP, Letcher B et al. Sustainable data analysis with Snakemake. _F1000Research_. 2021, 10:33)

[X]: <https://doi.org/10.1016/S0378-1119(02)00423-7> (What about frame-shifts?)

[N]: <http://jmlr.org/papers/v12/pedregosa11a.html> (Pedregosa F, Varoquaux G, Gramfort A, Michel V, Thirion B, Grisel O, et al. Scikit-learn: Machine Learning in Python. _Journal of Machine Learning Research_. 2011; **12**:2825--2830)
