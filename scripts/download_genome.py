#!/usr/bin/env python3
"""
This script downloads information from the ncbi FTP site:

* CDS as a fasta file
* Their translation in amino-acids as a fasta file
* Genomic annotations as a GFF file
* A file containing md5 sums

It pre-processes part of it in order to generate a table
associating old locus tags to new ones.

Data location and keywords identifying old locus tags
can be provided in a .yaml configuration file.
"""

import logging
import sys
from ftplib import FTP
from ftplib import all_errors as ftp_errors
from gzip import open as gzopen
from hashlib import md5
from pathlib import Path
# gff format downloaded from ncbi ftp contains "URL quoting" ("%2C" -> ",")
from urllib.parse import unquote
from  yaml import dump as ydump
from  yaml import safe_load as yload


logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


###################################
# Parsing genetic codes from NCBI #
###################################
CODONS = [f"{c1}{c2}{c3}" for c1 in "TCAG" for c2 in "TCAG" for c3 in "TCAG"]


def parse_gc(gc_fh):
    "Extract a piece of genetic code info from opened file *gc_fh*."
    line = gc_fh.readline().strip()
    assert line == "{"
    code_info = {}
    while not (line := gc_fh.readline().strip()).startswith("}"):
        if not line.startswith("--"):
            key, *valparts = line.split()
            if key == "id":
                code_info["id"] = valparts[0].strip(",")
            if key == "ncbieaa":
                code_info["codons"] = dict(zip(
                    CODONS,
                    valparts[0].rstrip(",")[1:-1]))
            if key == "sncbieaa":
                code_info["start_codons"] = dict(zip(
                    CODONS,
                    # valparts[0].rstrip(",")[1:-1])
                    valparts[0][1:-1]))
    return (code_info, line.strip()[-1])


####################
# Parsing gff data #
####################

# Example formats
GFF = """NC_002505.1     RefSeq  region  1       2961149 .       +       .       ID=NC_002505.1:1..2961149;Dbxref=taxon:243277;Is_circular=true;Name=I;biotype=El Tor;chromosome=I;gbkey=Src;genome=chromosome;mol_type=genomic DNA;old-name=Vibrio cholerae O1 biovar eltor str. N16961;serotype=O1;strain=N16961
NC_002505.1     RefSeq  gene    372     806     .       -       .       ID=gene-VC_RS00005;Name=mioC;gbkey=Gene;gene=mioC;gene_biotype=protein_coding;locus_tag=VC_RS00005;old_locus_tag=VC0002%2CVC_0002
NC_002505.1     Protein Homology        CDS     372     806     .       -       0       ID=cds-WP_000581314.1;Parent=gene-VC_RS00005;Dbxref=Genbank:WP_000581314.1;Name=WP_000581314.1;gbkey=CDS;gene=mioC;inference=COORDINATES: similar to AA sequence:RefSeq:WP_000581313.1;locus_tag=VC_RS00005;product=FMN-binding protein MioC;protein_id=WP_000581314.1;transl_table=11
"""

GTF = """NC_002505.1     RefSeq  gene    372     806     .       -       .       gene_id "VC_RS00005"; transcript_id ""; gbkey "Gene"; gene "mioC"; gene_biotype "protein_coding"; locus_tag "VC_RS00005"; old_locus_tag "VC0002,VC_0002";
NC_002505.1     Protein Homology        CDS     375     806     .       -       0       gene_id "VC_RS00005"; transcript_id "unassigned_transcript_1"; gbkey "CDS"; gene "mioC"; inference "COORDINATES: similar to AA sequence:RefSeq:WP_000581313.1"; locus_tag "VC_RS00005"; product "FMN-binding protein MioC"; protein_id "WP_000581314.1"; transl_table "11";
"""


def get_attr_string(line):
    """
    Extract and pre-process the attributes column from a gff line.
    """
    fields = line.split("\t")
    if len(fields) != 9:
        raise ValueError(f"Malformed line?\n{line}\n")
    # replace needed because some entries have a Note attribute
    # containing "; " separated notes
    return unquote(fields[8]).replace("; ", ", ")


def parse_attributes(attr_string):
    """
    Parse an attributes column from a gff line.

    Yield (key, val) pairs
    """
    item_strings = attr_string.split(";")
    for item_string in item_strings:
        try:
            key, val = item_string.split("=")
            yield (key, val)
        except ValueError as err:
            logging.warning(str(err))
            logging.warning(
                "Malformed attribute?\n%s\nSkipping %s\n",
                attr_string, item_string)
            continue


# We'll use gff, which has a "cleaner" attributes column,
# but contains "URL quoting" ("%2C" -> ",")
def get_gff_attributes(attr_string):
    """
    Extract gff attributes from gff line *line* as a dictionnary.

    >>> get_gff_attributes('ID=gene-VC_RS00005;Name=mioC;gbkey=Gene;gene=mioC;gene_biotype=protein_coding;locus_tag=VC_RS00005;old_locus_tag=VC0002%2CVC_0002')
    {'ID': 'gene-VC_RS00005', 'Name': 'mioC', 'gbkey': 'Gene', 'gene': 'mioC', 'gene_biotype': 'protein_coding', 'locus_tag': 'VC_RS00005', 'old_locus_tag': 'VC0002,VC_0002'}
    """
    return dict(
        (key, val)
        for key, val
        in parse_attributes(attr_string))


def make_locus_tag_converter(
        gff_filepath,
        seq_id_kw="locus_tag", alt_tag_kw="old_locus_tag"):
    """
    Parse a gff file and return a dict
    that gives the old locus tag for a given new locus tag.

    The code assumes that the new locus tag is recorded
    in the *seq_id_kw* field.
    The field name for the old locus tag should be given
    with parameter *alt_tag_kw*.
    """
    # key: new style locus tag, value: old style locus tag
    alt_tags = {}
    with gzopen(gff_filepath, "rt") as gff_fh:
        for line in gff_fh:
            if line[0] == "#":
                continue
            try:
                attributes = get_gff_attributes(get_attr_string(line.strip()))
            except ValueError as err:
                logging.warning(
                    "%s\nSkipping this line...\n", str(err))
                continue
            if alt_tag_kw in attributes and seq_id_kw in attributes:
                alt_tags[attributes[seq_id_kw]] = attributes[
                    alt_tag_kw].split(",")[0]
    return alt_tags


###############################
# Getting files from ftp site #
###############################
def get_codon_tables(ftp_url, codon_tables, local_assembly_dir):
    """
    Download the file containg codon tables
    located at *codon_tables* on the FTP site at *ftp_url*.

    The file will be downloaded in *local_assembly_dir*.
    """
    genetic_codes = {}
    logging.info("Connecting to %s...", ftp_url)
    with FTP(ftp_url) as ftp:
        ftp.login()
        logging.info("Getting data from %s...", codon_tables.parent)
        ftp.cwd(str(codon_tables.parent))
        dest_file = local_assembly_dir.joinpath(codon_tables.name)
        with dest_file.open("wb") as dest_fh:
            ftp.retrbinary(f"RETR {codon_tables.name}", dest_fh.write)
        with dest_file.open("rt") as gc_fh:
            while not next(gc_fh).strip() == "Genetic-code-table ::= {":
                continue
            (gc_dict, end_str) = parse_gc(gc_fh)
            while end_str[-1] == ",":
                genetic_codes[gc_dict["id"]] = {
                    "codons": gc_dict["codons"],
                    "start_codons": gc_dict["start_codons"]}
                (gc_dict, end_str) = parse_gc(gc_fh)
            genetic_codes[gc_dict["id"]] = {
                "codons": gc_dict["codons"],
                "start_codons": gc_dict["start_codons"]}
    return genetic_codes


def get_checksums(ftp_url, ftp_assembly_dir, local_assembly_dir):
    """
    Download the file containg md5sums for the content of *ftp_assembly_dir*.

    The directory *ftp_assembly_dir* should exist on the FTP site at *ftp_url*.

    The file will be downloaded in *local_assembly_dir*, and its content will
    be parsed and returned as a dict {filename: md5sum}.
    """
    # key: file name, value: md5sum
    md5_refs = {}
    logging.info("Connecting to %s...", ftp_url)
    with FTP(ftp_url) as ftp:
        ftp.login()
        logging.info("Getting data from to %s...", ftp_assembly_dir)
        ftp.cwd(str(ftp_assembly_dir))
        dest_file = local_assembly_dir.joinpath("md5checksums.txt")
        with dest_file.open("wb") as dest_fh:
            ftp.retrbinary("RETR md5checksums.txt", dest_fh.write)
        with dest_file.open("rt") as md5_fh:
            for line in md5_fh:
                md5sum, filename = line.split()
                md5_refs[filename[2:]] = md5sum
    return md5_refs


def check_md5(filepath, expected_md5, block_size=2**20):
    """
    Tell whether the file at *filepath* has the correct md5sum.

    The correct md5sum should be provided as argument *expected_md5*.

    The file is read in chunks of size *block_size* (in bytes),
    in order to be able to compute its md5sum
    without reading the whole file at once.
    See https://stackoverflow.com/a/1131238/1878788
    """
    file_hash = md5()
    with filepath.open("rb") as fh_to_check:
        while chunk := fh_to_check.read(block_size):
            file_hash.update(chunk)
    return file_hash.hexdigest() == expected_md5


def get_files_from_ftp(
        ftp_url, ftp_assembly_dir, local_assembly_dir,
        needed_files, md5_refs):
    """
    Download files in *needed_files* from FTP directory *ftp_assembly_dir*.

    The directory *ftp_assembly_dir* should exist on the FTP site at *ftp_url*.
    The files are downloaded in*local_assembly_dir*.
    They are checked using md5sums provided in *md5_refs*.
    """
    # Repeat while not all needed files are downloaded and md5 checked
    while needed_files:
        logging.info("Needed files: %s", ", ".join(sorted(needed_files)))
        with FTP(ftp_url) as ftp:
            ftp.login()
            ftp.cwd(str(ftp_assembly_dir))
            for filename in ftp.nlst():
                logging.debug("Found %s", filename)
                if filename not in needed_files:
                    continue
                dest_file = local_assembly_dir.joinpath(filename)
                try:
                    with dest_file.open("wb") as dest_fh:
                        ftp.retrbinary(f"RETR {filename}", dest_fh.write)
                except ftp_errors:
                    continue
                if dest_file.exists() and check_md5(
                        dest_file, md5_refs[filename]):
                    needed_files.remove(filename)


def main():
    """
    Main function of the script.
    """
    # Default config is for Vibrio cholerae
    config = {
        "data_dir": "/pasteur/zeus/projets/p01/BioIT/17009_Baharoglu/Baharoglu/data/Vcholerae",
        "assembly_type": "GCF",
        "assembly_nums": ["000", "006", "745"],
        "assembly_version": "ASM674v1",
        "seq_id_kw": "locus_tag",
        "alt_tag_kw": "old_locus_tag",
        # "ftp_url": "ftp://ftp.ncbi.nlm.nih.gov"
        "ftp_url": "ftp.ncbi.nlm.nih.gov"}
    if len(sys.argv) > 1:
        config_path = sys.argv[1]
        with open(config_path) as config_fh:
            config.update(yload(config_fh))
    data_dir = Path(config["data_dir"])

    # Not needed, because assembly data contains cds fasta
    # from Bio import Entrez

    # Entrez.email = "bli@pasteur.fr"
    # chromosomes = ["NC_002505", "NC_002506"]

    # for chromosome in chromosomes:
    #     with Entrez.efetch(
    #             db="nucleotide", id=f"{chromosome}.1",
    #             rettype="gb", retmode="text") as net_handle:
    #         gb_out = data_dir.joinpath(f"{chromosome}.gbk")
    #         gb_out.write_text(net_handle.read())

    assembly_type = config["assembly_type"]
    assembly_nums = config["assembly_nums"]
    assembly_version = config["assembly_version"]
    # assembly = "GCF_000006745.1_ASM674v1"
    assembly = f"{assembly_type}_{''.join(assembly_nums)}.{assembly_version[-1]}_{assembly_version}"
    ftp_url = config["ftp_url"]
    codon_tables = Path("entrez").joinpath("misc", "data", "gc.prt")
    ftp_assembly_dir = Path("genomes").joinpath(
        "all", assembly_type, *assembly_nums, assembly)

    # This will be used to count codons
    cds_fasta = f"{assembly}_cds_from_genomic.fna.gz"
    # This may not be used
    translated_fasta = f"{assembly}_translated_cds.faa.gz"
    # This will be used to associate old-style locus tags
    # with new-style locus tags
    gff_file = f"{assembly}_genomic.gff.gz"

    needed_files = {cds_fasta, translated_fasta, gff_file}

    local_assembly_dir = data_dir.joinpath(assembly)
    local_assembly_dir.mkdir(parents=True, exist_ok=True)

    genetic_codes = get_codon_tables(
        ftp_url, codon_tables, local_assembly_dir)
    with local_assembly_dir.joinpath("genetic_codes.yaml").open("w") as gc_fh:
        gc_fh.write(ydump(genetic_codes, default_style='"'))
    get_files_from_ftp(
        ftp_url, ftp_assembly_dir, local_assembly_dir,
        needed_files,
        get_checksums(ftp_url, ftp_assembly_dir, local_assembly_dir))
    # Now we should have cds_fasta, translated_fasta and gff_file

    alt_tags = make_locus_tag_converter(
        local_assembly_dir.joinpath(gff_file),
        seq_id_kw=config["seq_id_kw"],
        alt_tag_kw=config["alt_tag_kw"])
    # Save it as a table (maybe a dict was not the best choice...)
    # tags_table = local_assembly_dir.joinpath("tags.tsv")
    with local_assembly_dir.joinpath("tags.tsv").open("w") as tags_fh:
        for new_tag, alt_tag in alt_tags.items():
            tags_fh.write(f"{new_tag}\t{alt_tag}\n")

    return 0


if __name__ == "__main__":
    sys.exit(main())
