# Copyright (C) 2021 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
major, minor = sys.version_info[:2]
if major < 3 or (major == 3 and minor < 6):
    sys.exit("Need at least python 3.6\n")
from pathlib import Path
from yaml import safe_load as yload
# python3 -m pip install pandas
import pandas as pd
# python3 -m pip install git+https://gitlab.pasteur.fr/bli/libworkflows.git
from libworkflows import column_converter, warn_context

out_dir = Path(config.get("out_dir", "codon_usage"))
data_dir = Path(config.get("data_dir", "data"))
species = config.get("species", "Vcholerae")
assembly = config.get("assembly", "GCF_000006745.1_ASM674v1")
# 11 is for bacteria
gc_num = config.get("gc_num", "11")
seq_id_kw = config.get("seq_id_kw", "locus_tag")
alt_tag_kw = config.get("alt_tag_kw", "old_locus_tag")
kwargs = config.get("kwargs", {})

# No need to send these computationally trivial rules to a compute node.
localrules: all

# Top rule, whose input drives the whole workflow.
# Snakemake will start to climb the consumer.input <- producer.output
# dependency graph from here.
rule all:
    input:
        out_dir.joinpath(f"{species}", f"{assembly}", "counts", "codon_counts.tsv"),


include: "functions/common.py"


rule count_codons:
    input:
        cds_fasta = data_dir.joinpath("{species}", "{assembly}", "{assembly}_cds_from_genomic.fna.gz"),
        aa_fasta = data_dir.joinpath("{species}", "{assembly}", "{assembly}_translated_cds.faa.gz"),
        # TODO: use this to check first bases (yaml.safe_load, get table 11 and start_codons, compare with first aa of aa_sequence)
        genetic_codes = data_dir.joinpath(f"{species}", f"{assembly}", "genetic_codes.yaml"),
        tags_table = data_dir.joinpath("{species}", "{assembly}", "tags.tsv"),
    output:
        codon_counts = out_dir.joinpath("{species}", "{assembly}", "counts", "codon_counts.tsv"),
    log:
        warnings = out_dir.joinpath("logs", "{species}", "{assembly}", "count_codons.log")
    run:
        with warn_context(log.warnings) as warn:
            # Build the counts table #
            ##########################
            # T: transpose from (codons x genes) to (genes x codons)
            # rename_axis(index=["locus_tag", "length", "start_codon"]): give names to the MultiIndex
            # reset_index(["length", "start_codon"]): to make "length" and "start_codon" normal columns
            with open(input.genetic_codes) as gc_fh:
                genetic_code = yload(gc_fh)[gc_num]
            counts = pd.DataFrame(
                dict(count_codons_in_fastx(input.cds_fasta, input.aa_fasta, genetic_code, seq_id_kw=seq_id_kw, **kwargs))).T.rename_axis(
                    index=[
                        seq_id_kw,
                        "chrom",
                        "start",
                        "end",
                        "length",
                        "start_codon",
                        "expected_start_aa",
                        "first_stop",
                        "nb_stops",
                        "start_upstream",
                        "end_downstream"]).reset_index([
                            "chrom",
                            "start",
                            "end",
                            "length",
                            "expected_start_aa",
                            "start_codon",
                            "first_stop",
                            "nb_stops",
                            "start_upstream",
                            "end_downstream"])
            # Add an old_locus_tag column #
            ###############################
            # Could be done when building the table line by line in the first place,
            # but would need to give access to the tag conversion dict
            # to the counting and parsing functions
            tags_table = {}
            with open(input.tags_table) as tags_fh:
                for line in tags_fh:
                    new_tag, old_tag = line.strip().split("\t")
                    tags_table[new_tag] = old_tag
            if seq_id_kw != alt_tag_kw:
                # Use the index to create new colum (default behaviour of column_converter)
                switch_tags = True
                if switch_tags:
                    # This moves old_locus_tag at the first column
                    counts.assign(**{alt_tag_kw: counts.apply(
                        column_converter(tags_table),
                        axis=1)}).reset_index(seq_id_kw).set_index(alt_tag_kw).to_csv(
                            output.codon_counts, index=True, sep="\t")
                else:
                    # This leaves old_locus_tag at the last column
                    counts.assign(**{old_locus_tag: counts.apply(
                        column_converter(tags_table),
                        axis=1)}).to_csv(
                            output.codon_counts, index=True, sep="\t")
            else:
                counts.to_csv(
                    output.codon_counts, index=True, sep="\t")
