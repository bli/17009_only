# Copyright (C) 2021 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Functions used in workflow/codon_usage.snakefile
"""

import sys
from collections import Counter
from itertools import product
from re import compile as recompile
# python3 -m pip install cytoolz
from cytoolz import partition_all
# python3 -m pip install mappy
from mappy import fastx_read
# python3 -m pip install pandas
import pandas as pd


CODONS = list(map("".join, product("ACGT", repeat=3)))
# Common start codons in bacteria
START_CODONS = {"ATG", "GTG", "TTG"}


def check_aas(aas, genetic_code, start_codon):
    """
    Do some checks on amino-acid sequence *aas*.

    *genetic_code* should be a dict with at least two elements:

    * At key "codons", a dict associating amino-acids to codons.

    * At key "start_codons", a dict associating amino-acids to codons
      when they are used as start codons, or "-" if the codon
      is not supposed to be used as start codon.

    Both dicts have value "*" for stop codons.

    Return the length of the aa sequence,
    the index of the first stop codon and a list of reports
    about fishy features.
    """
    # Cannot name it report, because it interferes with Snakemake.
    report_strs = []
    expected_start_aa = genetic_code["start_codons"][start_codon]
    if expected_start_aa in {"-", "*"}:
        report_strs.append(
            f"  Sequence starts with an unexpected codon: {start_codon}\n")
    assert aas[0] in {
        genetic_code["codons"][start_codon],
        expected_start_aa}
    prot_len = len(aas)
    first_stop_pos = aas.find("*")
    nb_stops = aas.count("*")
    if first_stop_pos != -1:
        report_strs.append(
            f"  Translation contains {nb_stops} stop codons "
            f"({(nb_stops / prot_len):.2f}%) "
            f"(translated cds length: {prot_len})\n"
            f"  The first one occurs at position {first_stop_pos + 1}.\n")
    return (prot_len, expected_start_aa, first_stop_pos, nb_stops, report_strs)


def startend2len(startend):
    """
    Compute the length corresponding to a piece of genomic location info.

    Return a tuple containing the start, end and length and two Booleans.
    The first one indicates whether the sequence might start upstream.
    The second one indicates whether the sequence might end downstream.
    Like in bed format, start is zero-based and end is 1-based.

    >>> startend2len("332675..333637")
    (332674, 333637, 963, False, False)
    >>> startend2len("<912451..>912851")
    (912450, 912851, 401, True, True)
    >>> startend2len("<1649222..1649395")
    (1649221, 1649395, 174, True, False)
    >>> startend2len("428007..>428216")
    (428006, 428216, 210, False, True)
    """
    start, end = startend.split("..")
    istart = int(start.lstrip("<")) - 1
    iend = int(end.lstrip(">"))
    return (
        istart, iend,
        iend - istart,
        start[0] == "<", end[0] == ">")


def location2len(location_info, prot_len=None):
    """
    Compute the expected length of a sequence based on its location info.

    Return a tuple containing the start, end and length and two Booleans.
    The first one indicates whether the sequence might start upstream.
    The second one indicates whether the sequence might end downstream.
    Like in bed format, start is zero-based and end is 1-based.

    >>> location2len("complement(372..806)")
    (371, 806, 435, False, False)
    >>> location2len("332675..333637")
    (332674, 333637, 963, False, False)
    >>> location2len("join(262362..262620,262620..263539)")
    (262361, 263539, 1179, False, False)
    >>> location2len("complement(join(1937519..1938438,1938438..1938696))")
    (1937518, 1938696, 1179, False, False)
    >>> location2len("complement(<912451..>912851)")
    (912450, 912851, 401, True, True)
    >>> location2len("<263548..264219")
    (263547, 264219, 672, True, False)
    >>> location2len("428007..>428216")
    (428007, 428216, 210, False, True)
    """
    if location_info.startswith("complement"):
        location_info = location_info[11:-1]
    if location_info.startswith("join"):
        location_info = location_info[5:-1]
    [
        starts, ends, lens,
        start_upstreams, end_downstreams] = zip(*map(
            startend2len, location_info.split(",")))
    start = min(starts)
    end = max(ends)
    expected_len = sum(lens)
    start_upstream = any(start_upstreams)
    end_downstream = any(end_downstreams)
    #[expected_len, start_upstream, end_downstream] = [
    #    sum(elem) for elem
    #    in zip(*map(startend2len, location_info.split(",")))]
    if prot_len and (expected_len // 3 != prot_len):
        raise ValueError(
            f"Incoherent CDS and protein lengths "
            f"({expected_len}, {prot_len}).\n")
    #return (
    #    start, end, expected_len,
    #    bool(start_upstream), bool(end_downstream))
    return (start, end, expected_len, start_upstream, end_downstream)


# comment_splitter = recompile(r"(\[[^\[\]]*\])")
comment_splitter = recompile(r"(\[[^=]*=[^=]*\])")


# Use keyword arguments in order to have an interface compatible with
# seq_id_parser argument of function count_codons_in_fastx
def parse_fasta_comments(seq_id_kw="locus_tag", **header_elems):
    """
    Extract some info from fasta comments such as the following:

    [gene=mioC] [locus_tag=VC_RS00005] [protein=FMN-binding protein MioC] [protein_id=WP_000581314.1] [location=complement(372..806)] [gbkey=CDS]

    Return a tuple with the following elements:
    1) seq_id
    2) 0-based start
    3) 1-based end
    4) expected length
    5) Boolean indicating whether the sequence might start upstream.
    6) Boolean indicating whether the sequence might end downstream.

    >>> parse_fasta_comments(comments="[gene=mioC] [locus_tag=VC_RS00005] [protein=FMN-binding protein MioC] [protein_id=WP_000581314.1] [location=complement(372..806)] [gbkey=CDS]")
    ('VC_RS00005', 371, 806, 435, False, False)
    >>> parse_fasta_comments(comments="[gene=birA] [locus_tag=VC_RS01550] [protein=bifunctional biotin--[acetyl-CoA-carboxylase] ligase/biotin operon repressor BirA] [protein_id=WP_000658158.1] [location=332675..333637] [gbkey=CDS]")
    ('VC_RS01550', 332674, 333637, 963, False, False)
    """
    info = {}
    for field in comment_splitter.findall(header_elems["comments"].strip()):
        if not (field[0] == "[" and field[-1] == "]"):
            raise ValueError(
                f"Unexpected field: {field}\n"
                f"Origin: {header_elems['comments']}\n")
        try:
            key, val = field[1:-1].split("=")
        except ValueError:
            sys.stderr.write(
                f"Malformed field {field} in {header_elems['comments']}\n")
            raise
        if key in {"gene", "protein_id", seq_id_kw, "location"}:
            assert key not in info
            info[key] = val
    if "prot_len" in info:
        return (
            info[seq_id_kw],
            *location2len(info["location"], info["prot_len"]))
    return (info.get(seq_id_kw, None), *location2len(info["location"]))
    # Not all sequences have a gene or protein_id info
    # try:
    #     return (info["gene"], info["protein_id"], info["locus_tag"])
    # except KeyError as err:
    #     sys.stderr.write(f"Missing info in {header_elems['comments']}")
    #     raise


def count_codons_in_seq(seq):
    """
    Count codons in DNA sequence *seq*.

    *seq* should contain only non-ambiguous DNA bases.

    A pandas.Series object is returned, where the codons are used as the index.
    """
    if len(seq) % 3:
        raise ValueError(
            f"Sequence length ({len(seq)}) is not a multiple of 3.\n")
    codon_counts = Counter(map("".join, partition_all(3, seq.upper())))
    assert all(len(codon) == 3 for codon in codon_counts)
    return pd.Series(codon_counts, index=CODONS, dtype='UInt64').fillna(0)


def count_codons_in_fastx(
        cds_fastx_path, aa_fastx_path, genetic_code,
        seq_id_parser=parse_fasta_comments,
        seq_id_kw="locus_tag", max_aas=None):
    """
    Generate pairs index, count
    """
    for ((name, seq, _, comments), (_, aas, _)) in zip(
            fastx_read(cds_fastx_path, read_comment=True),
            fastx_read(aa_fastx_path)):
        if max_aas is not None:
            max_nts = 3 * max_aas
            seq = seq[:max_nts]
            aas = aas[:max_aas]
        chrom = name.split("|")[1].split("_")[0]
        start_codon = seq.upper()[:3]
        (
            prot_len,
            expected_start_aa,
            first_stop_pos,
            nb_stops,
            report_strs) = check_aas(
                aas, genetic_code, start_codon)
        try:
            (
                seq_id, start, end, expected_len,
                start_upstream, end_downstream) = seq_id_parser(
                    comments=f"{comments} [{prot_len}]",
                    seq_id_kw=seq_id_kw)
        except ValueError as err:
            # Maybe should fail at this point?
            if str(err).startswith("Incoherent CDS and protein lengths "):
                sys.stderr.write(str(err))
            else:
                raise
        if start_upstream:
            report_strs.append("  Sequence may start upstream.\n")
        if end_downstream:
            report_strs.append("  Sequence may end downstream.\n")
        if max_aas is None:
            assert len(seq) == expected_len
        else:
            assert len(seq) <= expected_len
        if len(seq) % 3:
            report_strs.append(
                f"  Length ({len(seq)}) is not a multiple of 3 "
                "(ignoring last bases).\n")
        if seq_id is None:
            report_strs.append(
                f"  No {seq_id_kw} attribute (skipping this sequence).\n")
        sys.stderr.write(
            f"\nWarning signs for sequence {name}:\n"
            "\n".join(report_strs))
        if seq_id is not None:
            yield (
                (
                    seq_id, chrom, start, end, len(seq),
                    start_codon, expected_start_aa,
                    first_stop_pos + 1, nb_stops,
                    start_upstream, end_downstream),
                count_codons_in_seq(seq[:3 * (len(seq) // 3)]))


if __name__ == "__main__":
    import doctest
    doctest.testmod()
